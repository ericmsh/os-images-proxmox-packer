source "proxmox-iso" "linux-kickstart" {
  boot_command            = ["<up><tab> text inst.ks=http://{{ .HTTPIP }}:{{ .HTTPPort }}/ks-${var.os}.cfg<enter><wait>"]
  boot_wait               = "10s"
  iso_storage_pool        = "${var.proxmox_storage_pool}"
  cloud_init              = true
  cloud_init_storage_pool = "${var.proxmox_storage_pool}"
  cores                   = "2"
  memory                  = "4096"
  cpu_type                = "host"
  os                   = "l26"
  disks {
    #disk_size         = "50"
    disk_size         = "${var.proxmox_storage_disk_size}"
    format            = "${var.proxmox_storage_format}"
    storage_pool      = "${var.proxmox_storage_pool}"
    #storage_pool_type = "${var.proxmox_storage_pool_type}"
    type              = "scsi"
  }
  http_directory           = "kickstart"
  insecure_skip_tls_verify = true
  iso_file                 = "${var.proxmox_iso_pool}/${var.iso_image}"
  network_adapters {
    #bridge = "vmbr2"
    bridge = "${var.proxmox_nic_bridge}"
    vlan_tag = "${var.proxmox_nic_vlan_tag}"
    model  = "virtio"
  }
  node                 = "${var.proxmox_node}"
  scsi_controller      = "virtio-scsi-single"
  ssh_username         = "root"
  ssh_password         = "password"
  ssh_port             = 22
  ssh_timeout          = "15m"
  template_description = "${var.template_description}"
  template_name        = "${var.template_name}"
  unmount_iso          = true
  vm_id                = "${var.vm_id}"
}


build {
  sources = ["source.proxmox-iso.linux-kickstart"]

  provisioner "shell" {
    inline = var.post_prov_commands
  }
}

