## See https://www.packer.io/plugins/builders/proxmox/iso
#variable "proxmox_url" {
#  type    = string
#  default = "${env("PROXMOX_URL")}"
#  description = "Proxmox URL passed as environment variable"
#}
#
#variable "proxmox_username" {
#  type    = string
#  #default = "${env("PROXMOX_USERNAME")}"
#  default = "packer@pve!Packer-Token2"
#  description = "Proxmox username passed as environment variable"
#}
#
#variable "proxmox_token" {
#  type    = string
#  #default = "${env("PROXMOX_TOKEN")}"
#  default = "8dcc8124-8888-4ec2-bad2-9de289983846"
#  description = "Proxmox token id passed as environment variable"
#}

variable "proxmox_node" {
  type    = string
  default = "pve"
  description = "Which node in the Proxmox cluster to start the virtual machine on during creation"
}

variable "proxmox_iso_pool" {
  type    = string
  default = "NAS:iso"
}

variable "proxmox_storage_format" {
  type    = string
  default = "raw"
}

variable "proxmox_storage_disk_size" {
  type    = string
  default = "16"
  description = "Disk size in GB"
}

variable "proxmox_nic_bridge" {
  type    = string
  default = "vmbr2"
  description = "Bridge name"
}

variable "proxmox_nic_vlan_tag" {
  type    = string
  default = "80"
  description = "VLAN ID"
}

variable "proxmox_storage_pool" {
  type    = string
  default = "storage-ha"
}

#variable "proxmox_storage_pool_type" {
#  type    = string
#  default = "zfspool"
#}

variable "version" {
  type    = string
  default = ""
}

variable "os" {
  type    = string
  default = "${env("OS")}"
  description = "OS variable used in kickstart inst.ks"
}

variable "iso_image" {
  type    = string
  default = "rhel-baseos-9.0-beta-3-x86_64-dvd.iso"
}

variable "template_description" {
  type    = string
  default = "RHEL 9 Beta Template"
}

variable "template_name" {
  type    = string
  default = "rhel9-template-ci"
}

variable "vm_id" {
  type    = string
  default = "822"
  #default = "823"
}

variable "post_prov_commands" {
  type    = list(string)
  default = ["uname -a","date"]
}
