source "proxmox-iso" "linux-kickstart" {
  boot_command = ["c", "linux /casper/vmlinuz -- autoinstall ds='nocloud-net;s=http://{{ .HTTPIP }}:{{ .HTTPPort }}/'", "<enter><wait><wait>", "initrd /casper/initrd", "<enter><wait><wait>", "boot<enter>"]
  boot_wait    = "10s"

  #boot_command = [ "c", "linux /casper/vmlinuz --- autoinstall ds='nocloud-net;s=http://{{ .HTTPIP }}:{{ .HTTPPort }}/' ", "<enter><wait><wait><wait><wait>", "initrd /casper/initrd<enter><wait><wait><wait><wait>", "boot<enter>" ]
  iso_storage_pool        = "${var.proxmox_storage_pool}"
  cloud_init              = true
  cloud_init_storage_pool = "${var.proxmox_storage_pool}"
  cores                   = "2"
  memory                  = "4096"
  cpu_type                = "host"
  os                   = "l26"
  disks {
    disk_size         = "16G"
    format            = "${var.proxmox_storage_format}"
    storage_pool      = "${var.proxmox_storage_pool}"
    #storage_pool_type = "${var.proxmox_storage_pool_type}"
    type              = "scsi"
  }
  http_directory           = "http"
  insecure_skip_tls_verify = true
  iso_file                 = "${var.proxmox_iso_pool}/${var.iso_image}"
  network_adapters {
    bridge = "vmbr1"
    model  = "virtio"
  }
  node                 = "${var.proxmox_node}"
  scsi_controller      = "virtio-scsi-single"
  ssh_username         = "ubuntu"
  ssh_password         = "ubuntu"
  ssh_port             = 22
  ssh_timeout          = "15m"
  template_description = "${var.template_description}"
  template_name        = "${var.template_name}"
  unmount_iso          = true
  vm_id                = "${var.vm_id}"
}


build {
  sources = ["source.proxmox-iso.linux-kickstart"]

  provisioner "shell" {
    inline = var.post_prov_commands
  }
}

