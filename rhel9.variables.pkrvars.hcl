iso_image            = "rhel-9.3-x86_64-dvd.iso"
template_description = "RHEL 9 Image generated by Packer - Contains cloud-init disk"
template_name        = "rhel9-template-ci"
vm_id                = "820"
proxmox_storage_pool = "raid1"
proxmox_iso_pool     = "Truenas:iso"
proxmox_nic_bridge   = "vmbr2"
proxmox_nic_vlan_tag = "80"
proxmox_storage_disk_size = "50"

post_prov_commands = ["subscription-manager register --org 12345678 --activationkey my-activationkey",
                      "yum update -y",
                      "yum install -y cloud-init qemu-guest-agent cloud-utils-growpart gdisk",
                      "subscription-manager unregister",
                      "yum clean all",
                      "shred -u /etc/ssh/*_key /etc/ssh/*_key.pub",
                      "rm -f /var/run/utmp",
                      ">/var/log/lastlog",
                      ">/var/log/wtmp",
                      ">/var/log/btmp",
                      "rm -rf /tmp/* /var/tmp/*",
                      "unset HISTFILE; rm -rf /home/*/.*history /root/.*history",
                      "rm -f /root/*ks",
                      "passwd -d root",
                      "passwd -l root"]

