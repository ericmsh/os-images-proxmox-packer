#!/bin/bash

export OS=$1
if [[ ! -a ./${OS}.variables.pkrvars.hcl ]]
then
   echo "Error"
   exit 1
fi

export PROXMOX_URL=https://pve.example.com:8006/api2/json
export PROXMOX_USERNAME="packer@pve"
export PROXMOX_PASSWORD="Changeme123"
#export PROXMOX_USERNAME='packer@pve!Packer-Token2'
#export PROXMOX_TOKEN="ldksjfhlaksdjh-8888-4ec2-bad2-9de289983846"

export PACKER_LOG=1

echo "Running packer validate for $OS"
echo "==================================="
packer validate -var-file=./${OS}.variables.pkrvars.hcl . || exit 1

echo "Running packer build for $OS"
echo "==================================="
packer build    -var-file=./${OS}.variables.pkrvars.hcl . || exit 1

